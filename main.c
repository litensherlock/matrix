#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "matrix.h"

int main(int argc, char **argv)
{
	float ** matrix;
	int N;
	if((N = read_matrix(&matrix)) != -1){
		float ** inv = inverse(matrix,N);
		if(inv != 0){
			print_matrix(inv,N);
			free_matrix(inv,N);
		}	
	}
	
	free_matrix(matrix, N);
	return 0;
}