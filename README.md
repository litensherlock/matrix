matrix

The program takes a square matrix and prints it out again, the plan is to use this initial part to build matrix functions around. Specifically matrix inversion.

Input format:
<size> 
<int> <int> ... <int>
<int> <int> ... <int>
.
.
.
<int> <int> ... <int>


example:
2
1 2
3 4

returns
1 2
3 4
