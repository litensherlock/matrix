#ifndef MY_MATRIX 
#define MY_MATRIX

int read_matrix(float *** _matrix);
void print_matrix(float ** matrix, int N);
void free_matrix(float ** matrix, int N);
float ** inverse(float ** in, int size);
void subtract_row(float * source, float * target, float scale, int size);
void divide_row(float denominator,float * row, int size);
int find_and_swap_up_row(float **, int row, int size);
int zero(float);

#endif