#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "matrix.h"

//returns the size of the matrix on success, otherwise -1
int read_matrix(float *** _matrix){
	int N;
	int r = scanf("%d",&N);
	
	if(r== EOF){
		printf("error reading input\n\rexiting");
		return -1;
	}else if (r == 0){
		printf("no input, please supply a matrix to stdin");
		return -1;
	}
	//we read the integer
	
	//create arrays to store the matrix in	
	float ** matrix = (float **)malloc((sizeof(float*)*N)); //square matrix N*N
	
	int i;
	for(i = 0; i < N; ++i){
		matrix[i] = (float*)malloc(sizeof(float)*N);
	}	
	
	for(i = 0; i < N; ++i){
		int j;
		for(j = 0;j < N; ++j){
			int r;
			int t = 0; //tries
			while((r = scanf("%f",&(matrix[i][j]))) == 0 && t < 100){
				t++;
			}
			if(r == EOF){
				printf("error reading input on row %d and column %d \n\rexiting",i,j);
				free_matrix(matrix,N);
				return -1;
			} else if(r == 0){
				printf("failed to read input after multiple tries\n\r");
				free_matrix(matrix,N);
				return -1;
			}
		}
	}
	*_matrix = matrix;
	return N;
}


//takes a matrix and returns a pointer to a invers matrix if it exists
//the input is modified, if no inverse is found it returns a null pointer
float ** inverse(float ** in, int size){
	//create the identiy matrix which we use to get the inverse
	float ** out = malloc(sizeof(float *)*size);
	int i;
	for(i = 0; i < size; i++){
		out[i] = malloc(sizeof(float)*size);
		int j;
		for(j = 0;j < size; j++){
			out[i][j] = 0.0;
		}
		out[i][i] = 1.0;
	}
		
	//Gaussian elimination step
	for(i = 0; i < size; i++){
		
		if(zero(in[i][i])){ // "equals" zero
			if(!find_and_swap_up_row(in, i, size)){
				printf("matrix not inversable\n\r");
				free_matrix(out, size);
				return 0;
			}
			find_and_swap_up_row(out, i, size);
		}
		// here we have something in in[i][i] to work with
		
		//scale the row so that [i][i] == 1
		divide_row(in[i][i], out[i], size);
		divide_row(in[i][i], in[i], size);		
		
		//zero out the column below
		int j;
		for(j = i + 1; j < size; j++){
			subtract_row(out[i], out[j], in[j][i], size); 
			subtract_row(in[i], in[j], in[j][i], size); // in row, out/target row, scale the in row, size
			
		}
	}
	
	
	//back substitution step
	int column;
	for(column = size - 1; column >= 1; column--){
		int row;
		for(row = column-1; row >= 0; row--){
			float factor = in[row][column];

			int j;
			for(j = 0; j < size; j++){
				out[row][j] = out[row][j] - factor*out[column][j];
				in[row][j] = in[row][j] - factor*in[column][j];
			}			
		}
	}
	
	return out;			
}

int zero(float f){
	//printf("(%f => %d)",f,abs(f*1e5) < 1);
	if(abs(f*1e5) < 1)
		return 1;
	else return 0;
}

// in row, out/target row, scale the in row, size
void subtract_row(float * source, float * target, float scale, int size){
	int i;
	for(i = 0;i < size; i++){
		target[i] = target[i] - (source[i] * scale);
	}
}

void divide_row(float denominator,float * row, int size){
	int i;
	for(i = 0;i < size; i++){
		row[i] = row[i]/denominator;
	}
}

int find_and_swap_up_row(float ** a, int row, int size){
	int i;
	for(i = row + 1;i < size; i++){
		
		//swap rows
		if(!zero(a[row][i])){
			float * temp = a[i];
			a[i] = a[row];
			a[row] = temp;
			return 1;
		}
	}
	
	return 0;
}

void print_matrix(float ** matrix, int N){
	int i;
	for(i = 0; i < N; ++i){
		int j;
		for(j = 0;j < N; ++j){
			printf("%f ",matrix[i][j]);
		}
		printf("\n\r");
	}
	printf("\n\r");
}


void free_matrix(float ** matrix, int N){
	int i;
	for(i = 0; i < N; ++i){
		free(matrix[i]);
	}
	free(matrix);
}